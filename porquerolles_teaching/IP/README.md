Python Workshop - Image Processing
===================================


## Course concepts

*Aims:*
A. Learn how to automate the processing and analysis of images by Python scripting
B. Measure co-localisation of motor proteins and adaptor protein in rat fibroblasts.
C. Segmentation and quantification of 2D confocal fluorescence microscopy images of a membranemarker in confluent epithelium-like cells.

1. **Python concepts to reinforce:**
	* Importing packages and modules
	* Reading files
	* Data and variable types
	* Importing data
	* Storing data in variables
	* Defining and using functions
	* Arrays, indexing, slicing
	* Control flow
	* Plotting images
	* Debugging by printing
	* Output formatting and writing files
	* Using the documentation


2. **Image processing, segmentation, analysis, automation:**	* Images as numbers		* Bit/colour depth		* Colour maps and look up tables 
	* Definition of Bio-image Analysis		* Image Analysis definition for signal processing science 
		* Image Analysis definition for biology		* Algorithms and Workflows		* Typical workflows in biology	* Convolution and Filtering		* Why do we do filtering?		* Convolution in 1D, 2D and 3D 
	* Pre-segmentation filtering		* De-noising		* Smoothening 
		* Unsharp mask	* Post-segmentation filtering		* Tuning segmented structures		* Mathematical Morphology * Erosion / dilation			* Distance map 
			* Watershed	* Analysis		* Extract measurements from segmentation	* Automation of pipeline, for all files in a directory

3. **Advanced material:**
	* CellProfiler to automate image analysis workflows and python plugin module	* (Optional) Code Optimisation (vectorisation, multiprocessing, cluster processing)

		
## Instructors
- Karin Sasaki
    - EMBL Centre for Biological Modelling
    - Organiser of course, practical materials preparation, tutor, TA
- Jonas Hartmann
    - Gilmour Lab, CBB, EMBL
    - Pipeline developer, practical materials preparation, tutor, TA
- Kora Miura
    - EMBL Centre for Molecular and Cellular Imaging
    - Tutor
- Volker Hilsenstein
    - Scientific officer at the ALMF
    - Tutor, TA (image processing)
- Toby Hodges
    - Bio-IT, EMBL
    - TA (python)
- Aliaksandr Halavatyi
    - Postdoc at the Pepperkik group
    - TA (programming/image processing)
- Imre Gaspar
    - Staff scientists at the Ephrussi group
    - TA (programming/image processing)


## Instructions on following this course
- Start with the pre-tutorial folder. There you will find three tutorials, one on numpy and arrays, one on python functions and one on a short image processing pipeline.
- Next, from the main tutorial folder, follow the pipeline first and then the batch pipeline. You have a choice of looking at a version with partial solutions, for both tutorials. This folder also contains the full solutions and original pipeline from which the tutorials were adapted.
- Finally, the advanced content has pipelines in cluster computation, data analysis, multi processing and vectorisation. These files are not in tutorial format and are provided as examples.


## Feedback 
We welcome any feedback on this course! 