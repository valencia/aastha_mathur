{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Python Programming"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Input and Output"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Opening Files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now know most of the Python data types (actually, just some - there are a lot more and, if you like, you can make your own as well) and have an idea of the ways we can repeat actions on lists and dictionaries with for loops, as well as take decisions based on our data using if statements. All of this requires a lot of data and so far, I have made you type the data into the programs by hand, which is a bit cruel. For this worksheet, we will be using a larger dataset (still tiny by many standards) and you can download a file containing the data from [GitHub](https://github.com/tobyhodges/ITPP/blob/master/speciesDistribution.txt). Just right-click 'Raw' at the top of the file content and download/save the linked file into the same directory as you are keeping the Python scripts."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, this requires that we know how to get data out of the file and into our Python program and that is what we are going to do in this worksheet. Specifically we are talking about reading data out of text files. Binary files face their own challenges, and I am not going to get into that in this course since handling them is very dependent on the implementation of the binary file. In any case, for a number of significant classes of binary files, such as images, BAM files or NetCDF formatted data, there are already Python modules to enable you to access the data in a simple way. But in any case, we will look at text files for now and firstly we need to know how to open them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have downloaded the file, you should make sure that it is saved into the same folder where you are going to save the python programs that you will use to analyse it. We will start simple, just by opening the file at the Python shell prompt."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "f = open('speciesDistribution.txt', 'r')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The file is now open, and `f` is now a variable referring to a file data type.  Obviously, the file argument for `open` is a string containing the filename, but the `'r'` probably needs to be explained. This argument is called the file mode, and `'r'` means that you only want to read data. If you specify `'w'`, it means that you want to write data into the file, which we will talk about later. One very important point is that when you open a file that already exists for writing, the contents of the file are cleared, and can’t be recovered. If you instead want to append data to an existing file you should specify `'a'` as the mode. If you specify `'r+'` then you can read and write to the file. These are the same regardless of the operating system that you are working on, but Windows has a few specific ones of it’s own, which you shouldn’t use if you can avoid them. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you might expect by now, files have their own methods and you can use these to read data from them.  The easiest way of doing this is to use `.readlines()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "lines = f.readlines()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The variable lines now refers to a list of strings containing each of the lines in the file. Try looking at one or two of them. If you didn’t look at the contents of the file before you opened it with your program, have a look at it now. If you compare `lines[1]` in Python with the second line in the file, you will see some differences. Most obvious is the presence of a `\\n` at the end of each line in the Python list. These are ‘newline characters’ and we need to remember to remove these when we process the data from the file. Although it looks like two characters, it is what is called an escape character, that is just a single character but one which we cannot normally see in a string. On most of the other lines there is another escape `\\t`, which is a tab character. Again, we need to remember this for use later. Tabs are often used to separate data items on the lines of text files because, amongst other reasons, they are much less likely to occur within the data than spaces."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Getting Data from Files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using `.readlines()` to create a list containing all of the lines is nice and simple, but has a major drawback. It’s fine when your file is small enough to read all of the lines into memory, but if you are reading a 32Gb SAM file, you are likely to run into problems. Here, you want to read one line at a time, and process it. Python files do have a `.readline()` method that will read one line, but it’s best to just use a `for` loop. Python has an idea of 'iterable' data types which you can put into `for` loops. We have seen two of these so far: the list and the dictionary. For a list you get each element in turn, and for a dictionary you get each key in turn. Strings are also iterable and return each character in turn. The point of mentioning this now is that files are also iterable, and Python tries to pass you exactly what we want: one line at a time. So we can start to write a program now to start processing this data file. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A brief aside: this will be the largest program you have written so far, and what I do when I am embarking on writing a large program is to start with just the basic structure and make sure that works then add to the program step by step and keep running it to make sure it is doing what I expect before it gets too complicated.  So to begin, in an editor window:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "datafile = open('speciesDistribution.txt', 'r')\n",
    "for line in datafile:\n",
    "    print line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "OK, so far so good, the program is basically printing the whole file out to the Python Shell window. However, I forgot about the newline characters at the end of the lines. You have probably noticed that the `print` statement automatically adds a newline to the end of everything it prints, so now we are getting two after each line, which is why the output is double-spaced. So the first thing to do is to fix that, by removing the newline characters from the lines as we read them in. Strings have a `.strip()` method which removes any newlines, spaces or tabs (we called these characters 'whitespace') at the start and end of each line. So add the line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`line = line.strip()`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "to the loop before the print statement (at the correct level of indentation) and try the program again.  Now the output should look single spaced."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "datafile = open('speciesDistribution.txt', 'r')\n",
    "for line in datafile:\n",
    "    line = line.strip()\n",
    "    print line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Processing the File"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we look again at the file, we can see that it consists of two types of data. Some lines contain the names of sampling sites and some contain a letter and a number. The letters are taxon designators and the numbers represent abundance of that taxon at that particular site (in this case, as measured by high-throughput DNA sequencing of 18S rRNA). We need to process the two line types differently and store the information in a suitable data structure."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take a moment to think about how you think we might go about doing that, and what the best data structure type to use might be for storing the taxon codes and counts for each site. Don’t worry if you find this a little confusing and/or daunting: we are going to work through it one step at a time, starting by identifying each site described in the data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The lines with the site names in them all start with the substring `Site:`, so they are easy to recognise.  We can use the string’s `.startswith()` method in an if statement to identify these lines so that we can process them separately.  Try using this method at the Python command line so you understand how it works before putting it into the program."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 3.1_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Change the program to only print out the lines that start with `Site:`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once that works, remove the `Site:` substring (and the space that follows it) from the string and just print the actual site name. Make sure that you store the name in a variable at this point as well - we will need it later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Starting to Build the Data Structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have isolated the site names we can think some more about what kind of data structures we will use to store the data we read from the file. When we work with data, it is important to design the data structures that we are going to use to store the data within the program. If we don’t get this right, we run the risk of having to write complex programs that might have been very much simpler if the data model were better. Another reason for wanting to understand complex data structures is that many Python modules use them and you will need to know what they mean when you come across them. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, we have some named sites and then some data corresponding to those sites. That to me sounds like a dictionary. The data we have for each site consists of several lines, which each contain a taxon code (the letter) and a count for that taxon. Again, this sounds like a dictionary. So we need a dictionary keyed by each site name, for which the associated value is another dictionary, keyed by the taxon IDs with values that are the counts for that site. So we need to create a dictionary of dictionaries. As with the whole program, it’s probably best to start simple."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to create the dictionary before we can populate it with the data from the file. We do this by defining an empty dictionary. You can do this by putting the line"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sites = {}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "just before the start of the loop that reads the file. This is often referred to as “initialising” a data structure, and is a strategy that you will use a lot when working with data read into Python from other sources. Now every time you find a new site in the file, you need to create the entry in this dictionary for that site name. Again, the value associated with this site name needs to be a new, empty dictionary. The example below shows how you can extract the site name from a line and create a new dictionary for it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "datafile = open('speciesDistribution.txt', 'r')\n",
    "for line in datafile:\n",
    "    line = line.strip()\n",
    "    if line.startswith('Site: '):  # you should have come up with something similar to\n",
    "        siteName = line[6:]        # this as your solution to exercise 3.1 ...\n",
    "        sites[siteName] = {}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 3.2_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Change your program to create the empty dictionaries as above, then, right at the end outside the loop, get it to print out the keys for the sites dictionary. These should be all of the site names."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Splitting Lines and Converting Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we are creating a dictionary for each site, we just need to parse the taxon/count lines from the file and put them into the appropriate dictionary for their site. As is common, on these lines we have two items of data. (We know too that once we see one of these lines we must also have the site name, which we have kept in a variable since it was extracted from the `Site:` line.) We can split the line as we did before to get the separate fields. In this case there will be two fields and they are returned as a list, but we can unpack them directly into individual variables if we want to in the assignment statement. So, after inserting an `else:` statement to go with the `if` statement that contains the `.startswith()` test to find the site names, you could type (again with the appropriate level of indentation): "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`taxonID, count = line.split()`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just a couple of words of caution.  Firstly, this will split the string on all whitespace characters.  This is fine in our case, but if any of your data were to contain spaces (for example if the single letter taxon names were classic binomial species names like _Homo sapiens_ instead), they would be split too.  You can limit to just tabs with:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`taxonID, count = line.split(‘\\t’)`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That’s solved the first problem. The second issue here is the data types. Type the following at the Python shell prompt:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "line = 'A\\t29304'\n",
    "taxonID, count = line.split('\\t')\n",
    "count"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "count = count + 99"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "count = 29304\n",
    "count"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "count = count + 99"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first time that Python prints the value of count, it has quotation marks around it, and you get an error when you try to add 99 to it. The second time it doesn’t have quotation marks and you don’t receive an error when adding 99. This is because the first time, the value of count is not a number but a string representing the number. Perl programmers don’t have to worry about this kind of thing, because Perl will automatically convert things for you when it thinks it needs to. With Python we have to be a bit more careful and convert the data ourselves.  This is done with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "count = int(count)\n",
    "count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "to convert to an integer and, if needed, you could convert it back again with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "count = str(count)\n",
    "count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now when you add the lines to your program, you have variables containing the site name, the taxonID and the count (which you can now make sure is converted to a proper integer). You can put these into the dictionary of dictionaries like this: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sites[siteName][taxonID] = count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this statement, `sites[siteName]` refers to the dictionary we created for that site, so we can just append another subscript onto it to get a reference to the data item for this taxon in that site dictionary. Hopefully, that makes some sense. Now, finally, all of the data from the file is where we want it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 3.3_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make the changes and make sure your program runs without errors.  We will also need another change, to keep track of the names/IDs of taxa as we encounter them.  At the top of the program, create a new empty list of taxon IDs e.g.,:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "taxa = []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, when you add the count to the dictionary of dictionaries, check if the taxon ID is in this new list and add it if not (just like you did when merging the shopping lists in Worksheet 2).  We will then have a non-redundant list of taxon names to play with in a minute."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Filling in the Blanks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unfortunately, there is a problem with this data. Some of the taxa were not detected in some of the sampling sites, so they do not have counts associated with them. This means that if we were, say, to plot the data in bar charts, some would have fewer bars than others or the bars would be in different positions, rather than just having a gap (or zero-height bar) where the taxon wasn’t found. What you need to do to avoid this is create new entries with counts of zero for the missing taxa at each site. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 3.4_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Put the zero values in the data structure. To do this you will need to loop through the sites, and for each site, loop through the IDs in the full non-redundant taxon list and if a taxon ID is not in the keys of the dictionary for the site, add it with a count of zero. Then you will need to check your program is working correctly. A good way to do that is described in the next section."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Formatting Data Structures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you are building up data structures like this, they can get very complex and it’s difficult to keep track and be sure that you are putting everything in the right place. Fortunately, there is a Python module (part of the standard library), which lets you print out the data in a comprehensible way. Of course, you could just print the entire data structure in one statement and this works, but it can be hard to read - there is no formatting at all - and it often doesn’t really help.  The `pprint` module formats the data in a hierarchical way, making it easier to understand. At the top of your program, you need to import the `pprint` module with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import pprint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You then create a formatter that will do the work for you with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "pp = pprint.PrettyPrinter(indent=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, when you want to check a data structure, you can just do the following and get a nice readable printout of your data structure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "variable = studentNumbers # this is the dictionary from Worksheet 2\n",
    "pp.pprint(variable)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare this output to the way that the same dictionary is displayed by the default `print` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print studentNumbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I hope you'll agree that the `pprint` version is much easier to interpret by eye."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 3.5_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the `pprint` module to dump out the contents of you data structure and check that the data corresponds with what you thought it should look like."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plotting Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To have a look at the data, we will be using the `pyplot` library from the `matplotlib` module. To do this, you will need to have a couple of new modules installed which don’t come with the standard installation of Python. The first is `numpy`, which defines a new array data type which you can use in ways that will be familiar if you use _R_ or _MATLAB_ and is really just there because the second, `matplotlib`, relies heavily on it. `matplotlib` gives a whole range of graph plotting functions again similar to the facilities in _MATLAB_. We won’t use `numpy` directly, but I encourage you to play with `matplotlib`, and in particular the `pyplot` parts of it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`numpy` and `matplotlib` are included in the Anaconda and Enthought distributions of Python, so __if you are running either of these distributions you don't need to worry about the installation step described below__. If you are running the ActivePython distribution on Windows, in the ActivePython program group there is an item for the Python Package Manager.  You can use this to install the packages. Open the Package Manager and a command window will appear, into which you need to type: "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`pypm install numpy`  \n",
    "`pypm install matplotlib`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That’s it. (If you are working on a different operating system and/or distribution, ask for help and we will find a way for you to install the packages that you need.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have the modules installed you can exit the Package Manager and at the Python prompt type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These statements make the functions, variables and classes of the two modules available to your program, but keep them at arms length, in their own 'namespaces'. This is to make sure that none of the names they use clash with anything in your program. It does mean that you have to type the prefix `np.` or `plt.` when you need to call them, but that’s not much of a price to pay for the safety that namespaces give you. If you are feeling reckless, you could, for example, have typed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`from numpy import *`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and then you wouldn’t have to deal with the prefix. Don’t do that. _Ever._ One day it will trip you up and it will take weeks to find out exactly what you have done wrong. Not that I’m talking from experience, or anything..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Anyway, we now have the modules loaded and ready to go, so we can get on with trying to plot a bar chart of our data.  We will generate some fairly pretty plots. `pyplot` is a bit unusual as a Python module, because it doesn’t define objects and methods for you, it’s just a set of function calls. Normally, you might expect to create a figure object, then use it’s methods, to e.g., add data or change the layout. However, `pyplot` just remembers what you are doing and performs on the last figure or subplot that you used. This type of interface is called “stateful”. If you really don’t like working this way, there is an object interface as well, but the documentation isn’t quite as good for it. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In our case, though, we want to start by creating a new figure (this is actually optional, but good practice - remember Python says “Explicit is better that implicit”)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.figure(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As soon as you executed this line, a new window might have appeared (it depends on your operating system) and this is where the figure will be drawn. Don’t worry if the window didn’t appear at this stage - it should show up when you’re done building the figure instead. Inline plotting is switched on in the IPython Notebook, so as we add things to the plot, you will see them appear below. The number argument to `plt.figure` is just a reference and lets you switch back to this figure later if you need to. The next step is to create a subplot. This is mostly used for figures with multiple panels, where each panel is a subplot. In this case, we’ll just do one for now, so we can just type one of the following statements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.subplot(1,1,1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.subplot(111)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These are again optional if you only have one panel in your figure, but again it’s generally better to be explicit. The arguments for the second form are (rows, columns, subfig), specifying the number of rows of panels, the number of columns and which one you want to draw now (which is in the range 1 - rows\\*colums). So if you have twelve panels and you wanted to select the seventh, you would use one of"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.subplot(3,4,7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.subplot(4,3,7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "depending on how many rows and columns you used. Subfigures/subplots are numbered as if read like text: left-right and top-bottom. If rows\\*columns is less than 10, you can use the top form without the commas e.g. `(321)` instead of `(3,2,1)`, but you might consider this to be less explicit."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can make up some data and plot it as a bar chart. The first thing is to create a list containing the heights of some bars, for which we can use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "barHeights = range(20,0,-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to specify where each of the bars will be drawn. This is the x-coordinate of the bottom left corner of the bar. For this we will use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "positions = range(20)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can draw the bar itself with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "plt.bar(positions, barHeights)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "plt.show() # you might need to run this line for your plot to appear"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "and the chart should appear in the window. You will notice that the figure has rescaled to fit the data and the axes are just labelled with numbers. There are lots of ways to tweak the figures, such as the `plt.xlabel()` and `plt.ylabel()` methods, which put titles on the axes, or `plt.title()`, which puts a title on the plot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, what we really want to do is plot the data from our file of sites and taxa. To make it as easy as possible to compare the distributions at the different sites, we want to plot all of the data in a single figure. To help us do this, we are going to define a function to plot the figure, then we can pass the data for each figure into the function, one site at a time. We haven’t done this before, so I will give you the code for the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def doBarChart(heights, labels, title, rows, columns, subplot):\n",
    "    plt.subplot(rows, columns, subplot)\n",
    "    plt.bar(range(len(labels)), heights)\n",
    "    plt.title(title)\n",
    "    plt.xlabel('Taxon')\n",
    "    plt.ylabel('Abundance')\n",
    "    plt.axis([0, len(labels), 0, 25000])\n",
    "    tickPos = []\n",
    "    for pos in range(len(labels)):\n",
    "        tickPos.append(pos+0.4)\n",
    "    plt.xticks(tickPos,labels)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This function takes a list of bar heights, a list of labels, a string to use as the title, then the number of rows and columns and the subplot number. First, the function passes these last three to `plt.subplot()` to initialise a new subplot on the figure. It then draws the bar chart, adds the title, labels the axes, and sets the minimum and maximum values for the axes. The last fiddly bit is to create a new list for the labels. By default, when you draw a bar chart, the widths of the bars are 0.8 units. So, we create a new list of positions which are offset by 0.4 units, so that the labels will centred relative to the bars. You can now add this function definition to the top of your program file along with the import statements and you will have everything you need to create the multi-panel figure. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### _Exercise 3.6_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot all of the data that you read from the file earlier into a single figure of bar plots for each site. This is challenging, but take it bit by bit and you should be able to do it.  \n",
    "* You will need to start by calling `plt.figure(1)`, then start a loop over the sites. \n",
    "* In that loop, you need to \n",
    "  * gather the data for the bar heights (the counts),\n",
    "  * then call the `doBarChart` function, passing in the row heights, labels, the site name as a title and the number of rows, columns and subplot number. \n",
    "* Once you have it working like that, try changing the program so that the sites and taxon IDs are in alphabetical order. \n",
    "* Then use the plot configuration window to tidy up the figure and save it.  \n",
    "\n",
    "You should be able to get it to look as below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you have got this working, try changing the bar colours. If you are really adventurous, plot all of the data on a single set of axes, with the data interleaved and the bars for different sites in different colours."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After you have finished on this exercise, or if you get really stuck and need to look at a solution, take a look at [this notebook](http://nbviewer.ipython.org/github/tobyhodges/ITPP/blob/v2/Exercise3_6WalkthroughMPL.ipynb), which runs through my way of producing the site plots with a different bar color for each taxon."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Summary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Files are opened with the `open()` command, and this returns a file object.\n",
    "* Methods of the file object, such as `.readline()` or `.readlines()` can be used to get data from the file.\n",
    "* Files can also be used as iterable data type in `for` statements (and other contexts).\n",
    "* Python doesn’t convert data types automatically, so you need to use functions like `str()` and `int()` to convert between strings and numbers.\n",
    "* The elements of data types like lists or dictionaries can themselves be things like lists or tuples or dictionaries, allowing arbitrarily complex data structures to be built up.\n",
    "* Python modules provide additional functionality for the language, and can perform many common data analysis tasks. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
