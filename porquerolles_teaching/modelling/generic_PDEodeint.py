# -*- coding: utf-8 -*-
"""
Created on Tue Oct 06 22:56:34 2015

@author:    Jonas Hartmann

@descript:  Generic PDE simulation using scipy's odeint (kind of a hack)
"""

# dA/dt = Q1 + (a * A) / (b * I) - R1 * A + D1 * d2A/d2x
# dI/dt = Q2 + c * A - R2 * I + D2 * d2A/d2x

# Parameters
Q1 = 0.0
Q2 = 0.0
a = 0.5
b = 3.0
c = 0.001
R1 = 0.02
R2 = 0.08
D1 = 0.01
D2 = 0.4
params = (Q1,Q2,a,b,c,R1,R2,D1,D2)

# Initial conditions (in space)
space = 60
C_0 = 1
import numpy as np
ini = np.ones((2,space)) * C_0
ini = ini + 0.1 * np.random.random((2,space))
ini = np.concatenate((ini[0,:],ini[1,:])) # Trick to pass space to odeint

# Time
dt = 1.0
import numpy as np
t = np.arange(0,2000,dt)

# Second differential
def diff2(V):
    
    d2V = np.zeros_like(V)
    d2V[0] = V[1] - V[0]
    d2V[-1] = V[-2] - V[-1]
    
    for i in range(1,len(V[:-1])):
        d2V[i] = V[i+1] + V[i-1] - 2*V[i] 
    
    return d2V
    

# PDE system (in format used for odeint)
def myODE(u,t,params):
    Q1,Q2,a,b,c,R1,R2,D1,D2 = params
    
    # Unpacking the concatenated spaces
    A = u[:space]
    I = u[space:]
    
    dA = Q1 + (a * A**2) / (b * I) - R1 * A + D1 * diff2(A)
    dI = Q2 + c * A**2 - R2 * I + D2 * diff2(I)
        
    return np.concatenate((dA,dI))
    
    
# Solve using odeint
from scipy.integrate import odeint
solution = odeint(myODE,ini,t,args=(params,))   
Xt = solution[:,:space]
Yt = solution[:,space:]


# Show timepoints
import matplotlib.pyplot as plt
#points = np.linspace(0,len(t),5)
#for point in points:
#    plt.plot(range(space),Xt[point,:],'red')
#    plt.plot(range(space),Yt[point,:],'blue')
#    plt.ylim([0,np.max(solution)])
#    plt.show()


### Show movie
import matplotlib.animation as ani

# Update image function
def update_img(p):
    num, fig, X_step, Y_step = p
    plt.cla()
    plt.plot(range(space),X_step,'green')
    plt.plot(range(space),Y_step,'red')
    plt.ylim([0,np.max(solution)])
    fig.canvas.set_window_title('Step {:}'.format(num))

# Initialize figure
fig = plt.figure()

# Zip up parameters
p = []
for i in range(len(t)):
    p.append((i,fig,Xt[i,:],Yt[i,:]))
ani_fig = ani.FuncAnimation(fig,update_img,p,interval=50,repeat_delay=10000)
plt.show()










