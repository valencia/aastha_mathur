# -*- coding: utf-8 -*-
"""
Created on Tue Oct 06 22:56:34 2015

@author:    Jonas Hartmann

@descript:  Generic ODE simulation using Euler integration.
"""

# dX/dt = q + a * Z - b * Y * X - r * X
# dY/dt = q + c * X - d * Z * Y - r * Y
# dZ/dt = q + e * Y - f * X * Z - r * Z

# Parameters
q = 0.1
r = 0.01
a = 0.5
b = 0.1
c = 0.5
d = 0.1
e = 0.5
f = 0.1
params = (q,r,a,b,c,d,e,f)

# Initial conditions
X_0 = 10
Y_0 = 5
Z_0 = 0.5

# Time
dt = 0.01
import numpy as np
t = np.arange(0,20,dt)
Xt = np.zeros_like(t)
Yt = np.zeros_like(t)
Zt = np.zeros_like(t)
Xt[0] = X_0
Yt[0] = Y_0
Zt[0] = Z_0

# ODE system
def myODE(X,Y,Z,params):
    q,r,a,b,c,d,e,f = params
    
    dX = q + a * Z - b * Y * X - r * X
    dY = q + c * X - d * Z * Y - r * Y
    dZ = q + e * Y - f * X * Z - r * Z
        
    return (dX,dY,dZ)
    
# Iterate
for step in range(1,len(t)):
    dX,dY,dZ = myODE(Xt[step-1],Yt[step-1],Zt[step-1],params)
    Xt[step] = Xt[step-1] + dX * dt
    Yt[step] = Yt[step-1] + dY * dt
    Zt[step] = Zt[step-1] + dZ * dt
    
# Show over time
import matplotlib.pyplot as plt
plt.plot(t,Xt,color='red')
plt.plot(t,Yt,color='blue')
plt.plot(t,Zt,color='green')
plt.show()
    
# Show as phase space trajectory
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.plot(Xt,Yt,Zt)
plt.show()











