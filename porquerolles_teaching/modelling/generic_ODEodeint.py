# -*- coding: utf-8 -*-
"""
Created on Tue Oct 06 22:56:34 2015

@author:    Jonas Hartmann

@descript:  Generic ODE simulation using scipy's odeint.
"""

# dX/dt = q + a * Z - b * Y * X - r * X
# dY/dt = q + c * X - d * Z * Y - r * Y
# dZ/dt = q + e * Y - f * X * Z - r * Z

# Parameters
q = 0.1
r = 0.01
a = 0.5
b = 0.1
c = 0.5
d = 0.1
e = 0.5
f = 0.1
params = (q,r,a,b,c,d,e,f)

# Initial conditions
X_0 = 10
Y_0 = 5
Z_0 = 0.5
ini = (X_0,Y_0,Z_0)

# Time
dt = 0.01
import numpy as np
t = np.arange(0,20,dt)

# ODE system (in format used for odeint)
def myODE(u,t,params):
    q,r,a,b,c,d,e,f = params
    X, Y, Z = u[0], u[1], u[2]    
    
    dX = q + a * Z - b * Y * X - r * X
    dY = q + c * X - d * Z * Y - r * Y
    dZ = q + e * Y - f * X * Z - r * Z
        
    return (dX,dY,dZ)
    
# Solve using odeint
from scipy.integrate import odeint
solution = odeint(myODE,ini,t,args=(params,))   
print np.shape(solution)
Xt = solution[:,0]
Yt = solution[:,1]
Zt = solution[:,2]
       
# Show over time
import matplotlib.pyplot as plt
plt.plot(t,Xt,color='red')
plt.plot(t,Yt,color='blue')
plt.plot(t,Zt,color='green')
plt.show()
    
# Show as phase space trajectory
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.plot(Xt,Yt,Zt)
plt.show()



