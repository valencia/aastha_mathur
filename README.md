# This is the README file!

## General rules and infos for this repo
- Create a new folder for each project
- To make changes, branch out with your name and then request a merge to master
- Place general code in the master branch
- Use project issues to discuss specific projects
- **Don't delete the repository! ;p** 
- Use a test repository (*not this one*) to find out how stuff works

## Ongoing projects
- Making the tutorials for porquerolles:
	- Basic Python programming - using Toby’s course as is (will not edit)
	- Stochastic and deterministic modelling
	- Image processing

## To do
- Aastha will look through all the material and decide what to (not) use.
- Make the modelling tutorials: Karin will start with the ODEs tutorial and Aastha with the stochastic tutorial
- Aastha will decide whether to edit the IP tutorial or use as is


## Completed projects

