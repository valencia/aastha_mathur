
I’ve fitted the following distributions, and plotted them imposed on the data histogram:
- exponential
- normal
- gamma
- weibull

and

For each distribution, I’ve plotted the following, superimposed on the equivalent for the data:
- Probability Distribution Functions (pdf) superimposed on the histogram of the data
- Cumulative Distribution Function (cdf)
- Probability plot

By comparing all of these, you check (visually) which model captures - or fails to capture - your data. 

The Goodness of Fit (GoF), like the chi squared, the Anderson Darling test and others, give you a p-value that can indicate ``how *good*’ the fit is, but this result is highly dependent on the architecture of the data, e.g. how many points you have, how spread it is, how far away are the outliers, etc. So it is hard to define what ‘best’ really means. If, after this, you still want to do a GoF,then I’ve included an example that Bernd (Stats Centre) gave me. You can follow that example in R, with your data.